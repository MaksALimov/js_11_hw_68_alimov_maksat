import axios from "axios";

export const TASK_TEXT = 'TASK_TEXT';
export const ADD_TASK = 'ADD_TASK';
export const DELETE = 'DELETE'

export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const ADD = 'ADD';
export const SUBTRACT = 'SUBTRACT';

export const FETCH_COUNTER_REQUEST = 'FETCH_COUNTER_REQUEST';
export const FETCH_COUNTER_SUCCESS = 'FETCH_COUNTER_SUCCESS';
export const FETCH_COUNTER_FAILURE = 'FETCH_COUNTER_FAILURE';

export const AXIOS_COUNTER_REQUEST = 'AXIOS_COUNTER_REQUEST';
export const AXIOS_COUNTER_SUCCESS = 'AXIOS_COUNTER_SUCCESS';
export const AXIOS_COUNTER_FAILURE = 'AXIOS_COUNTER_FAILURE';

export const TODO_REQUEST = 'TODO_REQUEST';
export const TODO_SUCCESS = 'TODO_SUCCESS';
export const TODO_FAILURE = 'TODO_FAILURE';

export const increase = () => ({type: INCREASE});
export const add = value => ({type: ADD, payload: value});
export const decrease = () => ({type: DECREASE});
export const subtracts = value => ({type: SUBTRACT, payload: value});

export const axiosCounterRequest = () => ({type: AXIOS_COUNTER_REQUEST});
export const axiosCounterSuccess = () => ({type: AXIOS_COUNTER_SUCCESS});
export const axiosCounterFailure = () => ({type: AXIOS_COUNTER_FAILURE});

export const fetchCounterRequest = () => ({type: FETCH_COUNTER_REQUEST});
export const fetchCounterSuccess = counter => ({type: FETCH_COUNTER_SUCCESS, payload: counter});
export const fetchCounterFailure = () => ({type: FETCH_COUNTER_FAILURE});

export const todoRequest = () => ({type: TODO_REQUEST});
export const todoSuccess = () => ({type: TODO_SUCCESS});
export const todoFailure = () => ({type: TODO_FAILURE});


export const fetchCounter = () => {
    return async (dispatch) => {
        dispatch(fetchCounterRequest());

        try {
            const response = await axios.get('https://redux-counter-js-11-default-rtdb.firebaseio.com/counter.json');
            const counters = Object.keys(response.data).map(id => {
                return response.data[id];
            });

            const counter = counters[0].counter

            if (response.data === null) {
                dispatch(fetchCounterSuccess(0));
            } else {
                dispatch(fetchCounterSuccess(counter))
            }
        } catch (e) {
            dispatch(fetchCounterFailure());
        }
    };
};

export const saveCounter = () => {
    return async (dispatch, getState) => {
        dispatch(axiosCounterRequest());

        try {
            const counter = getState().counter;
            const response = await axios.post('https://redux-counter-js-11-default-rtdb.firebaseio.com/counter.json', {counter: counter});

            if (response.data === null) {
                dispatch(axiosCounterSuccess());
            } else {
                dispatch(axiosCounterSuccess());
            }
        } catch (e) {
            dispatch(axiosCounterFailure());
        }
    };
};

export const saveTask = () => {
    return async (dispatch, getState) => {
        dispatch(todoRequest());

        const taskText = getState().taskText;
        try {
            const response = await axios.post('https://redux-counter-js-11-default-rtdb.firebaseio.com/tasks.json',{text: taskText});

            if (response.data === null) {
                dispatch(todoSuccess());
            } else {
                dispatch(todoSuccess());
            }
        } catch (e) {
            dispatch(todoFailure());
        }
    };
}
