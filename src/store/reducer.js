import {nanoid} from "nanoid";

import {
    ADD, ADD_TASK, AXIOS_COUNTER_FAILURE, AXIOS_COUNTER_REQUEST, AXIOS_COUNTER_SUCCESS,
    DECREASE, DELETE,
    FETCH_COUNTER_FAILURE,
    FETCH_COUNTER_REQUEST,
    FETCH_COUNTER_SUCCESS,
    INCREASE,
    SUBTRACT, TASK_TEXT, TODO_FAILURE, TODO_REQUEST, TODO_SUCCESS
} from "./actions";

const initialState = {
    counter: 0,
    loading: null,
    error: null,
    taskText: '',
    addTask: [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREASE:
            return {...state, counter: state.counter + 1};

        case DECREASE:
            return {...state, counter: state.counter - 1};

        case ADD:
            return {...state, counter: state.counter + action.payload};

        case SUBTRACT:
            return {...state, counter: state.counter - action.payload};

        case  FETCH_COUNTER_REQUEST:
            return {...state, error: null, loading: true};

        case FETCH_COUNTER_SUCCESS:
            return {...state, loading: false, counter: action.payload};

        case FETCH_COUNTER_FAILURE:
            return {...state, loading: false};

        case AXIOS_COUNTER_REQUEST:
            return {...state, error: null};

        case AXIOS_COUNTER_SUCCESS:
            return {...state};

        case AXIOS_COUNTER_FAILURE:
            return {...state};

        case TODO_REQUEST: {
            return {...state, error: null};
        }
        case TODO_SUCCESS: {
            return {...state};
        }

        case TODO_FAILURE: {
            return {...state};
        }
        case TASK_TEXT:
            return {...state, taskText: action.payload};

        case ADD_TASK:
            return {...state, addTask: [...state.addTask, {text: action.payload, id: nanoid()}]};

        case DELETE:
            return {...state, addTask: state.addTask.filter(task => task.id !== action.payload)};

        default:
            return state;
    }
};

export default reducer;
