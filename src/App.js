import Counter from "./containers/Counter/Counter";
import TodoList from "./containers/TodoList/TodoList";

const App = () => (
    <>
        <Counter/>
        <TodoList/>
    </>
);

export default App;
