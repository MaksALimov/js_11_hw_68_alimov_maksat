import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {ADD_TASK, DELETE, saveTask, TASK_TEXT} from "../../store/actions";
import './TodoList.css';

const TodoList = () => {
    const dispatch = useDispatch();
    const taskText = useSelector(state => state.taskText);
    const addTaskText = useSelector(state => state.addTask);

    useEffect(() => {
        dispatch(saveTask());
    }, [dispatch, addTaskText]);

    const addTask = e => {
        e.preventDefault();
        dispatch({type: ADD_TASK, payload: taskText});
    };

    const onInputChange = e => dispatch({type: TASK_TEXT, payload: e.target.value});

    const deleteTask = taskId => dispatch({type: DELETE, payload: taskId});

    return (
        <form onSubmit={addTask} className="TodoFormWrapper">
            <div className="TodoFormWrapper__task">
                <input
                    type="text"
                    value={taskText} onChange={e => onInputChange(e)}/>
                <button type="submit">Add</button>
            </div>
            {addTaskText.map(task => {
                return (
                    <div key={task.id} className="TodoFormWrapper__inner">
                        <li>{task.text}</li>
                        <button
                            type="button"
                            onClick={() => deleteTask(task.id)}
                            className="deleteBtn"
                        >
                            Delete Task
                        </button>
                    </div>
                )
            })}
        </form>
    );
};

export default TodoList;